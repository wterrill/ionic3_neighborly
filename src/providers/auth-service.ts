
import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GenericDatabase } from './generic-database'
import 'rxjs/add/operator/map';
//import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
//import { MessagingPage } from '../pages/messaging/messaging';
//import { Auth, IDetailedError, Push } from '@ionic/cloud-angular'; //User, UserDetails, PushToken
//import { Datasave } from '../providers/datasave';



export class User_local {
  email: string;
  firstName: string;
  lastName: string;
  building: string;
  garage: string;
  spot: string;
  unit: string;
  telephone: any;
  insurance: string;
  policy: string;
  make_model: string;
  body: string;
  color: string;
  plateState: string;
  plate: string;


  constructor(
    email: string, 
    firstName: string, 
    lastName: string, 
    building: string,
    garage: string, 
    spot: string, 
    unit: string, 
    telephone: any,
    insurance: string, 
    policy: string, 
    make_model: string,
    body: string, 
    color: string, 
    plateState: string, 
    plate: string, ) {

    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.building = building;
    this.garage = garage;
    this.spot = spot;
    this.unit = unit;
    this.telephone = telephone;
    this.insurance = insurance;
    this.policy = policy;
    this.make_model = make_model;
    this.body = body;
    this.color = color;
    this.plateState = plateState;
    this.plate = plate;
  }
}

@Injectable()
export class AuthService {

  private debugMode: boolean = false;

  protected currentUser: User_local;

  constructor(
    public http: HttpClient, 
    //public auth: Auth, 
    //public push: Push, 
    private database: GenericDatabase
    ) {
    if (this.debugMode) {
      console.log('Hello AuthService Provider');
    }
  }

  public login(credentials) {
    alert("there has to be an easier way....goddammit");
  }





  ///////

  // function tryAtMost(otherArgs, maxRetries, promise) {
  // 	promise = promise||new Promise();

  // 	// try doing the important thing

  // 	if(success) {
  // 		promise.resolve(result);
  // 	} else if (maxRetries > 0) {
  // 		// Try again if we haven't reached maxRetries yet
  // 		setTimeout(function() {
  // 			tryAtMost(otherArgs, maxRetries - 1, promise);
  // 		}, retryInterval);
  // 	} else {
  // 		promise.reject(error);
  // 	}

  ///////



  public register(credentials): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log("resolve");
      console.log(resolve);

      console.log("got to register");
      let result: any = "false";
      if (credentials.email === null || credentials.password === null) {
        //return Observable.throw("Please insert credentials");
        reject('Please insert valid credentials');
      } else {
/* AUTH AUTH AUTH AUTH
        this.auth.signup(credentials).then((data) => {
          console.log("auth provider data received");
          console.log(data);
          result = "true";
          resolve(result);
        },
          (err: IDetailedError<string[]>) => {
            for (let e of err.details) {
              if (this.debugMode) {
                console.log('The error details were: ', e);
              }
            }
            reject(err.details);
          });
*/

      }
    });
  }

  public getUserInfo(): User_local {
    return this.currentUser;
  }

  public storeUserInfo(email): void {

    var user = email.replace(/\./g,",");

    this.database.queryDB_single_item_as_Promise("users", user).then(result => {

      if (this.debugMode) {
        console.log('AuthProvider ~ Line 136:  Attempting to find the user, ' + user + ' in the database returned ', result);
      }

      this.currentUser = new User_local(email, '', '', '', '', '',
        '', '', '', '', '', '', '', '', '')

      for (var i = 0; i < result.length; i++) {

        if (this.debugMode) {
          console.log('result key, ', result[i].$key, '.  Result value, ', result[i].$value)
        }
        
        if (result[i].$key == 'lastName') {

          this.currentUser.lastName = result[i].$value;

        } else if (result[i].$key == 'firstName') {

          this.currentUser.firstName = result[i].$value;

        } else if (result[i].$key == 'color') {

          this.currentUser.color = result[i].$value;

        } else if (result[i].$key == 'building') {

          this.currentUser.building = result[i].$value;

        } else if (result[i].$key == 'garage') {

          this.currentUser.garage = result[i].$value;

        } else if (result[i].$key == 'insurance') {

          this.currentUser.insurance = result[i].$value;

        } else if (result[i].$key == 'policy') {

          this.currentUser.policy = result[i].$value;

        } else if (result[i].$key == 'spot') {

          this.currentUser.spot = result[i].$value;

        } else if (result[i].$key == 'unit') {

          this.currentUser.unit = result[i].$value;

        } else if (result[i].$key == 'make_model') {

          this.currentUser.make_model = result[i].$value;

        } else if (result[i].$key == 'plateState') {

          this.currentUser.plateState = result[i].$value;

        } else if (result[i].$key == 'plate') {

          this.currentUser.plate = result[i].$value;
        }
      }

      if (this.debugMode) {
        console.log('AuthService Stored the following User_local: ', this.currentUser);
      }

    }).catch(err => {
      console.log('AuthProvider ~ Line 181: Failed to find the user, Error: ', err);
    })
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }

   public sendResetEmail(email){ //: Promise<any> {
  //   // AUTH AUTH AUTH AUTH
  //   return new Promise((resolve, reject) => {
  //   //   this.auth.requestPasswordReset(email).then(result => {
  //   //     resolve();
  //   //   }).catch(err => {
  //   //     reject(err);
  //   //   })
  //   //  });
  }

   public submitReset(token, password){//: Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     this.auth.confirmPasswordReset(token, password).then(() => {
  //       resolve();
  //     }).catch(err => {
  //       reject(err);
  //     });

    //})
  }
}