import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';
import { GenericDatabase } from '../generic-database'


export class User_local {
  email: string;
  firstName: string;
  lastName: string;
  building: string;
  garage: string;
  spot: string;
  unit: string;
  telephone: any;
  insurance: string;
  policy: string;
  make_model: string;
  body: string;
  color: string;
  plateState: string;
  plate: string;


  constructor(
    email: string, 
    firstName: string, 
    lastName: string, 
    building: string,
    garage: string, 
    spot: string, 
    unit: string, 
    telephone: any,
    insurance: string, 
    policy: string, 
    make_model: string,
    body: string, 
    color: string, 
    plateState: string, 
    plate: string, ) {

    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.building = building;
    this.garage = garage;
    this.spot = spot;
    this.unit = unit;
    this.telephone = telephone;
    this.insurance = insurance;
    this.policy = policy;
    this.make_model = make_model;
    this.body = body;
    this.color = color;
    this.plateState = plateState;
    this.plate = plate;
  }
}

@Injectable()
export class AuthTestProvider {

  protected currentUser: User_local;
  private debugMode: boolean = false;

  constructor(
    public http: HttpClient,
    public afAuth: AngularFireAuth,
    private database: GenericDatabase
    ) {
    console.log('Hello AuthTestProvider Provider');
  }

  loginUser(newEmail: string, newPassword: string): Promise<any> {
    return this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword);
  }

  resetPassword(email: string): Promise<void> {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  logoutUser(): Promise<void> {
    return this.afAuth.auth.signOut();
  }

  signupUser(newEmail: string, newPassword: string): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword);
  }

  public getUserInfo(): User_local {
    return this.currentUser;
  }

  public storeUserInfo(email): void {

    var user = email.replace(/\./g,",");
    
    this.database.queryDB_single_item_as_Promise("users", user).then(result => {

      if (this.debugMode) {
        console.log('AuthProvider ~ Line 136:  Attempting to find the user, ' + user + ' in the database returned ', result);
      }

      this.currentUser = new User_local(email, '', '', '', '', '',
        '', '', '', '', '', '', '', '', '')

      var keys = Object.keys(result)

      for (var i = 0; i < keys.length; i++) {

        if (this.debugMode) {
          console.log('result key, ', keys[i], '.  Result value, ', result[keys[i]])
        }
        
        if (keys[i] == 'lastName') {

          this.currentUser.lastName = result[keys[i]];

        } else if (keys[i] == 'firstName') {

          this.currentUser.firstName = result[keys[i]];

        } else if (keys[i] == 'color') {

          this.currentUser.color = result[keys[i]];

        } else if (keys[i] == 'building') {

          this.currentUser.building = result[keys[i]];

        } else if (keys[i] == 'garage') {

          this.currentUser.garage = result[keys[i]];

        } else if (keys[i] == 'insurance') {

          this.currentUser.insurance = result[keys[i]];

        } else if (keys[i] == 'policy') {

          this.currentUser.policy = result[keys[i]];

        } else if (keys[i] == 'spot') {

          this.currentUser.spot = result[keys[i]];

        } else if (keys[i] == 'unit') {

          this.currentUser.unit = result[keys[i]];

        } else if (keys[i] == 'make_model') {

          this.currentUser.make_model = result[keys[i]];

        } else if (keys[i] == 'plateState') {

          this.currentUser.plateState = result[keys[i]];

        } else if (keys[i] == 'plate') {

          this.currentUser.plate = result[keys[i]];
        }
      }

      if (this.debugMode) {
        console.log('AuthService Stored the following User_local: ', this.currentUser);
      }

    }).catch(err => {
      console.log('AuthProvider ~ Line 181: Failed to find the user, Error: ', err);
    })
  }

}
