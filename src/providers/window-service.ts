import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

/*
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  https://forum.ionicframework.com/t/store-objects-in-localstorage/44788

  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  A WindowService that allows access to the window.localStorage

  It also wraps in several useful methods.  This uses the strict
  localStorage and is bound by its limitations (such as storage size).
  For larger data / storage needs, see the storage-service provider.

*/
@Injectable()
export class WindowService {

  private debugMode: boolean = false;
  public window = window;
  constructor(public http: HttpClient) {

  }

  setKey(key: string, value: string) {
    window.localStorage[key] = value;
  }

  getKey(key: string, defaultValue: string): string {
      return window.localStorage[key] || defaultValue;     
  }

  remove(key) {
    window.localStorage.removeItem(key);
  }



  setObject(key, value) {
    window.localStorage[key] = JSON.stringify(value);
    if (this.debugMode) {
      console.log(value);
    }
  }

  getObject(key, value) {
    return JSON.parse(window.localStorage[key] || '{}');
  }



}
