import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
//import { Database } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/onErrorResumeNext';
import { WindowService } from './window-service';
//import { AngularFireModule } from 'angularfire2';  // FirebaseListObservable, FirebaseObjectObservable
import { AngularFireDatabase } from 'angularfire2/database';
import { BUILDINGS } from '../components/library';
import { Transaction } from '../components/library';


/*
  Generated class for the DatastreamService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DatastreamService {

  private currentUser: string;

  /*

    IN AN ATTEMPT TO SIMPLIFY

    I am moving to a different observable
    for each page (I thought naming conventions that matched
    the individual pages would make more sense.)  The other reason
    I did this has to do with how we have to setup our datastream
    to provide for multiple buildings.  We could have just grabbed
    the database a level lower (in the page), but it would add another 
    level of mapping results and I was afraid that might cause more 
    confusion and complicate future troubleshooting as opposed to what
    I decided to do here.

    availableSpotsStream$: Should be subscribed too by the requests page
          to show all the available spots for rent.  MAPPING MUST STILL BE
          DONE IN THE REQUEST.TS PAGE BECAUSE OF THE NEED TO CONSTANTLY 
          MAP AND REMAP BASED ON START / STOP TIMES FOR THE QUERY.

    offeredSpotsStream$: Should be subscribed too by the offer page.  It 
          is mapped out here in the provider to provide only the spots which
          match the current logged in user.  It populates the data on the 
          offers page with the the users currently offered spot times.

    requestPendingReservedSpotsStream$:  Should be subscribed to by the
          (you guessed it) requestPendingReserved.ts page.  It shows the current
          users pending and confirmed requests.  Mapping should be implemented
          in the datastream-service provider.

    offerPendingReservedSpotsStream$:  Should be subscribed to by the
          offerPendingReserved.ts Page.  It shows the current users offers
          and the status of pending / reserved requests to use the owner's 
          available offers (available rental times for other users to park
          in the owners space).

  */

  public availableSpotsStream$: any;
  public offeredSpotsStream$: any;
  public requestPendingReservedSpotsStream$: any;
  public offerPendingReservedSpotsStream$: any;

  private debugMode: boolean = false;

  /**
   * No longer calling the initialize method through the constructor of the provider.
   * These streams are initialized after login (called by the constructor of the 
   * landing page.  I wanted to ensure that a user was logged in and we had the proper
   * building / garage info before moving onward and initializing database items.
   * 
   * @param http 
   * @param af 
   * @param windowService 
   */

  constructor(
    public http: HttpClient, 
    //private af: AngularFireModule, 
    private windowService: WindowService, 
    private afDatabase: AngularFireDatabase
    ) {
    if (this.debugMode) {
      console.log("Hello DatastreamService");
    }
    this.currentUser = this.windowService.getKey('user', ''); //gets the logged in user name

    // establishes the connection to the offers data stream
    // this.provideOfferedSpots();        //<------------------------------------------------------

  }

  public initializeStreams(): void {
    const BUILDING = this.windowService.getKey('building', '');
    const GARAGE = this.windowService.getKey('garage', '');     // temp holding incase it is needed....
    var path = 'spots';

    if (BUILDING == 'metropolitan') {
      path = BUILDINGS.METROPOLITAN_PLACE.BUILDING_DATABASE;
    } else if (BUILDING == 'terryaki') {
      path = BUILDINGS.TERRYAKI_PLACE.BUILDING_DATABASE;
    } else if (BUILDING == '1111WMadison') {
      path = BUILDINGS.ELEVEN_ELEVEN_W_MADISON.BUILDING_DATABASE;
    }else {
      // default case?
      path = 'spots';
    };

    if (this.debugMode) {
      console.log('initializeStreams in the datastream-service is searching for spots within the building ', BUILDING, ' at the full path: ', path);
    }
    this.provideSpots(path);
  }


  /**
   * This function provides a stream for four different observables
   * in the datastream-service.  It is called internally by the 
   * provideSpots function.
   * 
   * @param path The path to the observable that must be setup
   * as the stream
   */
  private provideSpots(path: string): void {
    if (this.debugMode) {
      console.log("Querying for spots from the building at path: ", path);
    }


   const listresultSetObservable = this.afDatabase.list(path).valueChanges();  //WLT added for new angularfire2

    // const listresultSetObservable = Observable.create(
    // x => this.afDatabase.list(path),
    // e =>  console.log('onError: %s', e),
    // () => console.log('onCompleted'));  
    
    listresultSetObservable.subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The datastream service query for provideAvailable returned: ");
        console.log(queryResultSet);
      }

      // MAP THE QUERYRESULT INTO AN ARRAY OF OFFERS
      var offeredSpotsBothGarages = this.combineGarages(queryResultSet);
      if (this.debugMode) {
        console.log("the offeredSpotsBothGarages returned: ");
        console.log(offeredSpotsBothGarages);
      }

      // setup the availablSpotsStream$
      //
      //
      // Futher mapping is done upon
      // query for an available space
      // in request.ts.  Just pass the
      // the aggregate along.
      var availableSpotAggregate = this.provideAvailable(offeredSpotsBothGarages);

      if (this.debugMode) {
        console.log("datastream-service ~ line 145....Creating the availableSpotsStream$ Observable using the availableSpotAggregate: ")
        console.log(availableSpotAggregate)
      }

      this.availableSpotsStream$ = Observable.create(observer => {
        try {
          if (this.debugMode) {
            console.log('datastream-service ~ line 151 .... successfully created the availableSpotsStream$ observable')
          }
          if (this.debugMode) {
            console.log("before observer.next in availableSpotsStream$:")
            console.log(availableSpotAggregate);
          }
            observer.next(availableSpotAggregate);
            if (this.debugMode) {
              console.log(availableSpotAggregate);
            }
          // observer.oncompleted();
        } catch (error) {
          console.log('datastream-service encountered an error, Error: ', error);
          Observable.throw(error);
        }
      });
      //
      //
      // end of availableSpotsStream$


      // setup the offeredSpotsStream$
      //
      //
      var offeredSpotsRefined = this.provideOffered(offeredSpotsBothGarages);
      if (this.debugMode) {
        console.log("Here is offeredSpotsRefined after 'provideOffered(offeredSpotsBothGarages)':");
        console.log(offeredSpotsRefined);
      }

      this.offeredSpotsStream$ = Observable.create(observer => {
        try {
          if (this.debugMode) {
            console.log('datastream-service ~ line 174 .... successfully created the offeredSpotsStream$ observable')
          }
          observer.next(offeredSpotsRefined);
          if (this.debugMode) {
          }
          //observer.oncompleted();
        } catch (error) {
          Observable.throw(error);
        }
      });
      //
      //
      // end of the offeredSpotsStream$

      // setup the offerPendingReservedSpotsStream$
      //
      //
      var allPendingReservedSpotsRefined = this.provideOfferPendingReserved(offeredSpotsBothGarages);

      this.offerPendingReservedSpotsStream$ = Observable.create(observer => {
        try {
          if (this.debugMode) {
            console.log('datastream-service ~ line 199 .... successfully created the offeredSpotsStream$ observable')
          }
          observer.next(allPendingReservedSpotsRefined.offerPendingReserved);
          if (this.debugMode) {
          }
          //observer.oncompleted();
        } catch (error) {
          console.log("error:");
          console.log(error)
          Observable.throw(error);
        }
      });
      //
      //
      // end of the offerPendingReservedSpotsStream$

      // setup the requestPendingReservedSpotsStream$
      //
      //

      this.requestPendingReservedSpotsStream$ = Observable.create(observer => {
        try {
          if (this.debugMode) {
            console.log('datastream-service ~ line 199 .... successfully created the offeredSpotsStream$ observable')
          }
          observer.next(allPendingReservedSpotsRefined.requestPendingReserved);
            if (this.debugMode) {

          }
          //observer.oncompleted();
        } catch (error) {
          Observable.throw(error);
        }
      });
      //
      //
      // end of the requestPendingReservedSpotsStream$

      // end of initial subscription
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
    


  }

  private provideAvailable(offeredSpotsAggregate: any): any {
    if (this.debugMode) {
      console.log("Mapping out available spots from the provided offeredSpotsAggregate: ")
      console.log(offeredSpotsAggregate);
    }

    return offeredSpotsAggregate;

  }




  /**
   * Sets up the offered spots by the current user.  
   */

  private provideOffered(offeredSpotsAggregate: any): any {
    /* First I try to remove all the spots from my stream that 
    do not have any offers.  If offers is not undefined then I
    push the entire response to a holding variable, aggregate.*/

    var offeredSpotsRefined = new Array();

    for (var i = 0; i < offeredSpotsAggregate.length; i++) {
      if (this.debugMode) {
        console.log('datastream-service investigating the offeredSpotsBothGarages ~ line 245:') 
        console.log(offeredSpotsAggregate[i], offeredSpotsAggregate);
      }
      
     
      if (offeredSpotsAggregate[i].owner == this.windowService.getKey('user', '')) {
        offeredSpotsRefined.push(offeredSpotsAggregate[i]);
      }
    }


    return offeredSpotsRefined;

  }

  private provideOfferPendingReserved(offeredSpotsAggregate): any {

    if (this.debugMode) {
      console.log('datastream-service - provideOfferPendingReserved _______________________ Entered with offeredSpotsAggregate of: ', offeredSpotsAggregate);
      console.log('checking against user -> ', this.windowService.getKey('user', ''));
    }

    var offerPendingSpotsRefined = new Array();
    var offerReservedSpotsRefined = new Array();

    var totalOfferPendingSpots = 0;
    var totalOfferReservedSpots = 0;

    var requestPendingSpotsRefined = new Array();
    var requestReservedSpotsRefined = new Array();

    var totalRequestPendingSpots = 0;
    var totalRequestReservedSpots = 0;

    for (var j = 0; j < offeredSpotsAggregate.length; j++) {

      if (offeredSpotsAggregate[j].offer_status == 2 && offeredSpotsAggregate[j].owner == this.windowService.getKey('user', '')) {

        if (this.debugMode) {
          console.log("Pushing offeredPendingSpotsRefined[" + j + "]");
          console.log(offeredSpotsAggregate[j]);
        }

        totalOfferPendingSpots++;

        offerPendingSpotsRefined.push(new Transaction(offeredSpotsAggregate[j]._1_offer_start, offeredSpotsAggregate[j]._2_offer_end,
          offeredSpotsAggregate[j]._3_request_start, offeredSpotsAggregate[j]._4_request_end,
          offeredSpotsAggregate[j].building, offeredSpotsAggregate[j].garage,
          offeredSpotsAggregate[j].offer_end, offeredSpotsAggregate[j].offer_start,
          offeredSpotsAggregate[j].offer_status, offeredSpotsAggregate[j].offer_weight,
          offeredSpotsAggregate[j].owner, offeredSpotsAggregate[j].request_end,
          offeredSpotsAggregate[j].request_start, offeredSpotsAggregate[j].request_user,
          offeredSpotsAggregate[j].request_weight, offeredSpotsAggregate[j].spot))

      } else if (offeredSpotsAggregate[j].offer_status == 3 && offeredSpotsAggregate[j].owner == this.windowService.getKey('user', '')) {

        if (this.debugMode) {
          console.log("Pushing offerReservedSpotsRefined[" + j + "]");
          console.log(offeredSpotsAggregate[j]);
        }

        totalOfferReservedSpots++;

        offerReservedSpotsRefined.push(new Transaction(offeredSpotsAggregate[j]._1_offer_start, offeredSpotsAggregate[j]._2_offer_end,
          offeredSpotsAggregate[j]._3_request_start, offeredSpotsAggregate[j]._4_request_end,
          offeredSpotsAggregate[j].building, offeredSpotsAggregate[j].garage,
          offeredSpotsAggregate[j].offer_end, offeredSpotsAggregate[j].offer_start,
          offeredSpotsAggregate[j].offer_status, offeredSpotsAggregate[j].offer_weight,
          offeredSpotsAggregate[j].owner, offeredSpotsAggregate[j].request_end,
          offeredSpotsAggregate[j].request_start, offeredSpotsAggregate[j].request_user,
          offeredSpotsAggregate[j].request_weight, offeredSpotsAggregate[j].spot))
      } else if (offeredSpotsAggregate[j].offer_status == 2 && offeredSpotsAggregate[j].request_user == this.windowService.getKey('user', '')) {

        if (this.debugMode) {
          console.log("Pushing requestPendingSpotsRefined[" + j + "]");
          console.log(offeredSpotsAggregate[j]);
        }

        totalRequestPendingSpots++;

        requestPendingSpotsRefined.push(new Transaction(offeredSpotsAggregate[j]._1_offer_start, offeredSpotsAggregate[j]._2_offer_end,
          offeredSpotsAggregate[j]._3_request_start, offeredSpotsAggregate[j]._4_request_end,
          offeredSpotsAggregate[j].building, offeredSpotsAggregate[j].garage,
          offeredSpotsAggregate[j].offer_end, offeredSpotsAggregate[j].offer_start,
          offeredSpotsAggregate[j].offer_status, offeredSpotsAggregate[j].offer_weight,
          offeredSpotsAggregate[j].owner, offeredSpotsAggregate[j].request_end,
          offeredSpotsAggregate[j].request_start, offeredSpotsAggregate[j].request_user,
          offeredSpotsAggregate[j].request_weight, offeredSpotsAggregate[j].spot))

      } else if (offeredSpotsAggregate[j].offer_status == 3 && offeredSpotsAggregate[j].request_user == this.windowService.getKey('user', '')) {


        if (this.debugMode) {
          console.log("Pushing requestReservedSpotsRefined[" + j + "]");
          console.log(offeredSpotsAggregate[j]);
        }

        totalRequestReservedSpots++;

        requestReservedSpotsRefined.push(new Transaction(offeredSpotsAggregate[j]._1_offer_start, offeredSpotsAggregate[j]._2_offer_end,
          offeredSpotsAggregate[j]._3_request_start, offeredSpotsAggregate[j]._4_request_end,
          offeredSpotsAggregate[j].building, offeredSpotsAggregate[j].garage,
          offeredSpotsAggregate[j].offer_end, offeredSpotsAggregate[j].offer_start,
          offeredSpotsAggregate[j].offer_status, offeredSpotsAggregate[j].offer_weight,
          offeredSpotsAggregate[j].owner, offeredSpotsAggregate[j].request_end,
          offeredSpotsAggregate[j].request_start, offeredSpotsAggregate[j].request_user,
          offeredSpotsAggregate[j].request_weight, offeredSpotsAggregate[j].spot))
      }
    }

    var offerPendingReservedSpotsAggregate = {
      pendingSpotsRefined: offerPendingSpotsRefined,
      reservedSpotsRefined: offerReservedSpotsRefined,
      totalPendingSpots: totalOfferPendingSpots,
      totalReservedSpots: totalOfferReservedSpots
    }


    var requestPendingReservedSpotsAggregate = {
      pendingSpotsRefined: requestPendingSpotsRefined,
      reservedSpotsRefined: requestReservedSpotsRefined,
      totalPendingSpots: totalRequestPendingSpots,
      totalReservedSpots: totalRequestReservedSpots
    }

    var allPendingReservedSpots = {
      offerPendingReserved: offerPendingReservedSpotsAggregate,
      requestPendingReserved: requestPendingReservedSpotsAggregate
    }

    if (this.debugMode) {
      console.log('datastream-service || pendingReserved stream variables : offerPendingReservedSpotsAggregate', offerPendingReservedSpotsAggregate,
        ' requestPendingReservedSpotsAggregate : ', requestPendingReservedSpotsAggregate, ' allPendingReservedSpots: ', allPendingReservedSpots)
    }

    return allPendingReservedSpots;
  }


  private combineGarages(garageArray): any {
    /*
   The original data came back as garage_name.spots.spot_number; we need to create an array at the level spot number.
   This code filters through the garages (garageArray[0] would be garrageArray[0].spots.spot_number) and leaves
   behind the array offeredSpotsBothGarages.  In this array, each index on the array represented spot_number so that
   it can be iterated through properly to build the observable used to manpiulate the spots.
   */
    var offeredSpotsBothGarages: {
      global_spot_status: string,
      owneruser: string,
      offers: any
    }[] = new Array();

    var offeredSpotsRefined = new Array();

    for (var i = 0; i < garageArray.length; i++) {


      if (this.debugMode) {
        console.log('datastream service - combineGarages(garageArray) ~ line 367: peeking at the garage array: ')
        console.log( garageArray, garageArray[i]);
      }
      if (garageArray[i].spots != undefined) {

        if (this.debugMode) {
          console.log("garageArray[i].spots")
          console.log(garageArray[i].spots)
        }

        var tempOfferArray = Object.keys(garageArray[i].spots).map(key => garageArray[i].spots[key])
        if (this.debugMode) {
          console.log("tempOfferArray")
          console.log(garageArray[i].spots)
        }
        if (tempOfferArray.length > 0) {
          for (var j = 0; j < tempOfferArray.length; j++) {
            if (tempOfferArray[j].offers != undefined) {
              offeredSpotsBothGarages.push({
                owneruser: tempOfferArray[j].owneruser, global_spot_status: tempOfferArray[j].global_spot_status,
                offers: tempOfferArray[j].offers
              })
              if (this.debugMode) {
                console.log("offeredSpotsBothGarages - if not undefined")
                console.log(offeredSpotsBothGarages)
              }
            }
          }
        } else {
          if (tempOfferArray[j].offers != undefined) {
            offeredSpotsBothGarages.push({
              owneruser: tempOfferArray[0].owneruser, global_spot_status: tempOfferArray[0].global_spot_status,
              offers: tempOfferArray[0].offers
            })
            if (this.debugMode) {
                console.log("offeredSpotsBothGarages - else")
                console.log(offeredSpotsBothGarages)
              }
          }
        }

        if (this.debugMode) {
          console.log('datastream-service - combineGarages is pushing a new offeredSpotsBothGarages: ')
          console.log(offeredSpotsBothGarages);
        }
      }
    }

    for (var i = 0; i < offeredSpotsBothGarages.length; i++) {

      var tempOfferArray = Object.keys(offeredSpotsBothGarages[i].offers).map(key => offeredSpotsBothGarages[i].offers[key]);
      if (this.debugMode) {
        console.log("datastream-service - combineGarage(garageArray) ~ line 401:  tempOfferArray", tempOfferArray);
      }
      if (tempOfferArray.length > 0) {
        for (var j = 0; j < tempOfferArray.length; j++) {


          /* let parentKey: any = Object.keys(offeredSpotAggregate[i].offers)[j];
           if (this.debugMode) {
             console.log("parentKey")
             console.log(parentKey);
           }*/

          offeredSpotsRefined.push(new Transaction(tempOfferArray[j]._1_offer_start, tempOfferArray[j]._2_offer_end,
            tempOfferArray[j]._3_request_start, tempOfferArray[j]._4_request_end,
            tempOfferArray[j].building, tempOfferArray[j].garage,
            tempOfferArray[j].offer_end, tempOfferArray[j].offer_start,
            tempOfferArray[j].offer_status, tempOfferArray[j].offer_weight,
            tempOfferArray[j].owner, tempOfferArray[j].request_end,
            tempOfferArray[j].request_start, tempOfferArray[j].request_user,
            tempOfferArray[j].request_weight, tempOfferArray[j].spot))
        }
      } else {

        offeredSpotsRefined.push(new Transaction(tempOfferArray[j]._1_offer_start, tempOfferArray[j]._2_offer_end,
          tempOfferArray[j]._3_request_start, tempOfferArray[j]._4_request_end,
          tempOfferArray[j].building, tempOfferArray[j].garage,
          tempOfferArray[j].offer_end, tempOfferArray[j].offer_start,
          tempOfferArray[j].offer_status, tempOfferArray[j].offer_weight,
          tempOfferArray[j].owner, tempOfferArray[j].request_end,
          tempOfferArray[j].request_start, tempOfferArray[j].request_user,
          tempOfferArray[j].request_weight, tempOfferArray[j].spot))

      }

    }
    if (this.debugMode) {
      console.log("Refined Offered Spots: ", offeredSpotsRefined);
    }

    return offeredSpotsRefined;

  }


  /**
   * DEPRECATED AS OF 0.04.00.  
   * 
   * USE STILL APPLICABLE FOR ALL VERSIONS
   * BELOW 0.04.00.  ANYTHING ABOVE 
   * 0.04.00 SHOULD RELY ON ABOVE METHODS.
   * 
   * TO BE DELETED.......
   */
  provideOfferedSpots(): void {
    if (this.debugMode) {
      console.log("Querying for spots whose global_spot_status == 1");
    }


//     // Create observer
// var observer = Rx.Observer.create(
//   x => console.log('onNext: %s', x),
//   e => console.log('onError: %s', e),
//   () => console.log('onCompleted'));


          // const listObservable = Observable.create(
          // x => this.afDatabase.list("/spots",  ref => ref.orderByChild('global_spot_status').equalTo(1)),
          // e => e => console.log('onError: %s', e),
          // () => console.log('onCompleted'));  

    const listObservable = this.afDatabase.list("/spots",  ref => ref.orderByChild('global_spot_status').equalTo(1)).valueChanges();

    listObservable.subscribe(queryResultSet => {
      if (this.debugMode) {
        console.log("The query returned: ");
        console.log(queryResultSet);
      }
      this.offeredSpotsStream$ = Observable.create(observer => {
        try {


          observer.next(queryResultSet);
          //observer.oncompleted();
        } catch (error) {
          Observable.throw(error);
        }
      })
    }, err => {
      console.log("Error with query.  Error code is: ");
      console.log(err);
    });
  }


}
