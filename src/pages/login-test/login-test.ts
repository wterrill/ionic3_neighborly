import { Component } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  LoadingController, 
  Loading, 
  AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthTestProvider } from '../../providers/auth-test/auth-test';
import { EmailValidator } from '../../validators/email-test';
import { Landing2Page } from '../landing2/landing2';

@IonicPage()
@Component({
  selector: 'page-login-test',
  templateUrl: 'login-test.html',
})
export class LoginTestPage {

  public loginForm:FormGroup;
  public loading:Loading;

  constructor(
    public navCtrl: NavController, 
    public authData: AuthTestProvider, 
    public formBuilder: FormBuilder, 
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

      this.loginForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });
  }

  loginUser(){
    if (!this.loginForm.valid){
      console.log(this.loginForm.value);
    } else {
      this.authData.loginUser(this.loginForm.value.email, this.loginForm.value.password)
      .then( authData => {
        this.navCtrl.setRoot(Landing2Page);  //HomePage   this.navCtrl.setRoot(Landing2Page, this.auth);
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }

  goToResetPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }

  createAccount(){
    this.navCtrl.push('SignupPage');
  }

}
