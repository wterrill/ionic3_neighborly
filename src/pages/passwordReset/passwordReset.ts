import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular'; //Loading
import { AuthTestProvider } from '../../providers/auth-test/auth-test';
import { EqualValidator } from '../../validators/equalValidator';
import { DebugMessage } from '../../components/library';
import { GenericDatabase } from '../../providers/generic-database';
import * as moment from 'moment';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-password-reset',
  templateUrl: 'passwordReset.html'
})

export class PasswordResetPage {

  resetForm: FormGroup;
  emailForm: FormGroup;

  submitAttempt: boolean = false;
  validSubmission: boolean = false;
  resetSuccess: boolean = false;

  private loading: Loading;

  private debugMode: boolean = false;

  private resetInformation = {
    email: '',
    password: '',
    resetToken: ''
  }

  constructor(
    private navCtrl: NavController,
    public authData: AuthTestProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public database: GenericDatabase,
    public formBuilder: FormBuilder
  ) {

    this.resetForm = formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, EqualValidator('password')]],
      resetToken: ['', [Validators.minLength(6), Validators.maxLength(6), Validators.required]]
    });

    this.emailForm = formBuilder.group({
      email: ['', Validators.compose([Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'), Validators.required])],
    });

  }

  sendResetPin() {
    console.log(this.emailForm.controls);
    this.submitAttempt = true;

    if (this.emailForm.valid) {

      if (this.debugMode) {
        console.log("success!")
        console.log(this.resetForm.value);
      }

      let debug = new DebugMessage("unknown", "password reset", JSON.stringify(this.resetInformation), moment().toLocaleString(), "valid password reset form filled");

      if (debug.debugOnOff) {
      //   this.database.update_DB_childByParent("/debug/", this.resetInformation.email.replace(/\./g,","), moment().unix().toString(), debug);
      }

      
      this.authData.resetPassword(this.resetInformation.email).then((user) => {

        this.validSubmission = true;
        //this.showPopup('Success', 'Your reset confirmation pin has been sent to ' + this.resetInformation.email);
        this.showPopup('Success', 'Your reset email as been sent to ' + this.resetInformation.email);
        this.gotoLogin();

      },
      (error) => {
        var errorMessage: string = error.message;
        let errorAlert = this.alertCtrl.create({
          message: errorMessage,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        errorAlert.present();
      }
      ).catch(err => {
        this.validSubmission = false;
        this.showPopup('>_^', 'We were unable to reset your password.  Please check your e-mail address and try again.  If the problem' +
          ' persists, please contact william.terrill@gmail.com or lbrockma34@gmail.com');
      })
    }
  } // end of Save()

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.resetSuccess) {
              this.navCtrl.push(LoginPage);
            }
          }
        }
      ]
    });
    alert.present();
  } //end of showPopup


  /**
 * Nothing here....
 */
  showLoading(message: string): void {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.present();
  }

  gotoLogin(){
    this.navCtrl.push(LoginPage);
  }
}



