/*


NOTE

NOT INVOLVED WITH THE MESSAGES-DATABASE PROVIDER CURRENTLY (03/15/17)

THIS IS THE OLD MESSAGE PAGE THAT SENDS A PUSH notification

WILL BE UPDATED AS A CREATE MESSAGE PAGE THAT SHOWS ALL users

AT A LATER DATE


*/



import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
//import { Http, Headers } from '@angular/http';
//import { Headers } from '@angular/http';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
//import { Rx } from "@reactivex/rxjs";
// Changing import Rx from 'rxjs/Rx'; to import * as Rx from 'rxjs/Rx'; the Rx causes an issue.
//import { Database, Auth } from '@ionic/cloud-angular';
import { MessageModalPage } from '../message-modal/message-modal';
import { ConversationsDatabase } from '../../providers/conversations-database';
import { WindowService } from '../../providers/window-service';
//import { FirebaseDatabase } from '../../providers/firebase-database';



@Component({
  selector: 'page-messaging',
  templateUrl: 'messaging.html'
})
export class MessagingPage {

  searchQuery: String = '';
  listItems: {
    username: String,
    image: String,
    id: any
  }[] = new Array();

  data: any;

  users: {
    email: String,
    image: String,
  }[] = new Array();

  currentUser: String;

  private debugMode: boolean = false;


  constructor(
    public navCtrl: NavController, 
    public http: HttpClient,
    //public db: Database, 
    //public auth: Auth, 
    public modalCtrl: ModalController, 
    private conversationDb: ConversationsDatabase,
    private windowService: WindowService) {
    if (this.debugMode) {
      console.log(windowService.window);
    }
    this.currentUser = windowService.getKey('user', '');

    this.listItems = new Array();
    //this.dbConnect();
    this.initializeItemsGet();
    console.log("entering messaging page");

  }



  // INITIALIZE THE DATABASE THROUGH A POST COMMAND
  // THERE IS ALSO A DATABASE OBJECT WHICH CAN BE USED
  // SEE dbConnect()

  initializeItemsGet() {
    var link = "https://api.ionic.io/users";
    let headers = new HttpHeaders();

    delete this.users;

    this.users = new Array();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdG'
      + 'kiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5'
      + 'ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs');
    this.http.get(link, { headers: headers })
      // .map maps the arrayed objects into an object
      // from the oberservable response.  We turn the response
      // into a JSON, where .data is used to filter objects based
      // on the first categorie from the ionic cloud response
      .map((response: any) => {
        return response.json().data.map((item) => {
          // we can prase the item a step further here if required
          return item;
        })
      })
      // .map the data as item, and pass it to the subscriber.
      .subscribe(data => {
        if (this.debugMode) {
          console.log(data);
        }
        // now iterate through the data objects ... this time we 
        // filter on the details category, and pull the email and image.
        for (var i = 0; i < data.length; i++) {

          // push a new object with a parameter email
          // and a parameter token (currently using image URL)
          // to the users array from the current data object
          if (this.currentUser != data[i].details.email.toString()) {
            this.users.push({
              email: data[i].details.email.toString(),
              image: data[i].details.image.toString()
            });
          }
        }
        for (var i = 0; i < this.users.length; i++) {
          this.listItems.push({ username: this.users[i].email.toString(), image: this.users[i].image.toString(), id: i });
          console.log(this.users[i].email.toString() + ', ' + this.users[i].image.toString());
        }

      }, (err) => {
        console.log(err);
      });
  }

  getItems(ev: any) {
    // reset the item list
    this.initializeItemsGet();
    // set val to the value of the search bar
    let val = ev.target.value;
    // if the value is an empty string don't filter the getItems
    if (val && val.trim() != '') {
      this.listItems = this.listItems.filter((item) => {
        return (item.username.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  /**
   * itemSelected takes the selected item and finds the existing
   * conversation or creates a new conversation.  Will add an entry
   * to the conversation collection with new chatId if the two users
   * have never chatted before.
   * 
   * @param listItem selected item from the message
   * screen.
   */
  itemSelected(listItem) {

    // variable datatype that we can pass to
    // the messaging page which stores are pertinent
    // message creation information
    var selectedUser: {
      user: any;
      chatId: string;
    } = { user: "null", chatId: "null" };

    var exists = this.conversationDb.conversationExists(listItem.username);
    if (this.debugMode) {
      console.log("listItem username is *-*-", listItem.username)
    }



    if (exists) {
      if (this.debugMode) {
        console.log("The conversation exists, pass chatId");
      }
      selectedUser.chatId = this.conversationDb.existingChatId;
      selectedUser.user = listItem.username;
    } else {
      // som basic string manipulation to remove @ and . from name
      // the ionic db services will not allow these characters in 
      // the name of a collection
      var user1First = this.currentUser.replace("@", "");
      var user1Second = user1First.replace(/\./g,"");

      var user2First = listItem.username.replace("@", "");
      var user2Second = user2First.replace(/\./g,"");

      selectedUser.chatId = user1Second.toString().toLowerCase() + "_" + user2Second.toString().toLowerCase();

      if (this.debugMode) {
        console.log("Attempt to insert the record into conversation collection.", );
      }
      this.conversationDb.createConversation(this.currentUser.toString(), listItem.username.toString().toLowerCase(), selectedUser.chatId);
    }
    if (this.debugMode) {
      console.log("Passing the selected user to the modal page: ", selectedUser);
    }
    selectedUser.user = listItem.username;
    this.conversationDb.reset();
    this.navCtrl.push(MessageModalPage, selectedUser);
  }



  /*  // CREATES A DATABASE CONNECTION.  TESTS FOR AUTHENTICATION FIRST
  // IF USER IS AUTHENTICATED, CONNECT TO 

  dbConnect() {

    var details = { email: "lbrockma34@gmail.com", password: "password1" };

    if (this.auth.isAuthenticated()) {
      const userCollect = this.db.collection("beer");
      this.db.connect();
      var userList = userCollect.order("id").fetch();
      console.log("I am authenticated as the walrus");
      console.log(JSON.stringify(userList.last.toString()));

    } else {
      this.auth.login('basic', details).then(() => {

        const userCollect = this.db.collection("beer");
        this.db.connect();
        var userList = userCollect.order("id").fetch();
        console.log("I am not authenticated, the penguin");
        console.log(JSON.stringify(userList.first.toString()));


      });
    }


  }*/

  // GATHERS BASIC FORM DATA FOR REGISTRATION -- SHOWS
  // EXAMPLES OF PASSING DATA FROM THE IONIC INPUTS TO
  // THE TS BACKEND

  registerBtn(fName, lName, eMail, invite) {

    var link = "https://api.ionic.io/push/notifications"
    //var d = new Date();
    let headers = new HttpHeaders();

    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyODc0ZWI2Yy0xNWIyLTRhMDgtODg5Ni0yNzAyY2I5ODQxNzYifQ.d6pZNB2-5GyDOl9ke3HUmPreA6DH06EZp7ni8_g6fNs');


    // var registerUser = {

    //   "name": fName,
    //   //"lName": lName,
    //   "food": eMail,
    //   "confirmed": invite,
    //   "signup_date": d.getDate()
    // }

    var payload = {
      "tokens": "foj0GHATkec:APA91bFYduDuE9DLAkI9pQIrmaieMKutKIopVzWBMDx9lrDe7n7N2xbohv_cK_MtgIoF8TNMU4CeSlhC9CvqvS7gZSOH8b9M27ps49O3UAmH6-60Q7AWQq_wGrWTvIgA1erWfCVSlkva",
      "profile": "push_notification",
      "notification": {
        "message": "Jerimiah was a bullfrog"
      }
    }

    //var jsonString = JSON.stringify(registerUser);
    var jsonString2 = JSON.stringify(payload);

    //alert(jsonString);
    this.http.post(link, jsonString2, { headers: headers })
      .subscribe((res: any) => {
        if (this.debugMode) {
          console.log(res.json());
        }
      }, (err) => {
        console.log(err);
      });
    if (this.debugMode) {
      console.log(jsonString2);
    }
    alert("sent");

  }

  delBtn(delRec) {
    var link = "http://52.25.18.254/api.php/potluck"
    alert(delRec);
    this.http.delete(link, delRec);
    // .subscribe(res => {
    // 	console.log(res.json());
    // }, (err) => {
    // 	console.log(err);
    // });
  }

}

