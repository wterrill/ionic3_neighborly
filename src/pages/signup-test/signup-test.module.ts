import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupTestPage } from './signup-test';

@NgModule({
  declarations: [
    SignupTestPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupTestPage),
  ],
})
export class SignupTestPageModule {}
