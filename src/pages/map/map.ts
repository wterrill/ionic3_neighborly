import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
//import { ImgMapComponent } from 'ng2-img-map';

//added by Will
//import {Events} from 'ionic-angular';

//https://plnkr.co/edit/Xwn1Z8HLDgQRwWwdXfoT?p=preview  -> image map code I followed
//had to do "npm install ng2-img-map"


/*
  Generated class for the Map page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  togglepic: boolean = false;
  

  public press: number = 0;
  public pan: number = 0;
  public swipe: number = 0;
  public tap: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log("entering map page");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

  pressEvent(e) {
    this.press++
  }

  panEvent(e) {
    this.pan++
  }
  swipeEvent(e) {
    this.swipe++
  }
  tapEvent(e) {
    this.tap++
    console.log("tapped");
    console.log(e);
    console.log(e.center.x);
    console.log(e.center.y);
  }

  picturetoggle(){
    console.log("toggling");
    if(this.togglepic === false)
    {
      this.togglepic = true;
      console.log("turning on");
    }
    else if(this.togglepic === true)
    {
      this.togglepic = false;
      console.log("turning off");
    }
  }
  
}
