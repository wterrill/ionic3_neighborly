    import { Component, ViewChild } from '@angular/core';
    import { FormBuilder, FormGroup, Validators } from '@angular/forms'
    import { SpotValidator } from '../../validators/spotValidator';
    import * as moment from 'moment'


    @Component({
      selector: 'page-signup',
      templateUrl: 'signup.html'
    })

    export class SignupPage2 {
      @ViewChild('signupSlider') signupSlider: any;
      signUpForm: FormGroup;

      private debugMode: boolean = false;
      registerCredentials = {
        spot: '',
      };

      constructor(
        public formBuilder: FormBuilder,
      ) {
        this.signUpForm = formBuilder.group({
          spot: ['', [SpotValidator.isValid]]
        });
        console.log("signUpForm");
        console.log(this.signUpForm);  
      }
    }



