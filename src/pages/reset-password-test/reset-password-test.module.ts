import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetPasswordTestPage } from './reset-password-test';

@NgModule({
  declarations: [
    ResetPasswordTestPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetPasswordTestPage),
  ],
})
export class ResetPasswordTestPageModule {}
