import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';



import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthTestProvider } from '../../providers/auth-test/auth-test';
import { EmailValidator } from '../../validators/email-test';



/**
 * Generated class for the ResetPasswordTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password-test',
  templateUrl: 'reset-password-test.html',
})
export class ResetPasswordTestPage {
  public resetPasswordForm:FormGroup;

  constructor(public authData: AuthTestProvider, public formBuilder: FormBuilder,
  public nav: NavController, public alertCtrl: AlertController) {

  this.resetPasswordForm = formBuilder.group({
    email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
  })
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordTestPage');
  }

  resetPassword(){
    if (!this.resetPasswordForm.valid){
      console.log(this.resetPasswordForm.value);
    } else {
      this.authData.resetPassword(this.resetPasswordForm.value.email)
      .then((user) => {
        let alert = this.alertCtrl.create({
          message: "We just sent you a reset link to your email",
          buttons: [
            {
              text: "Ok",
              role: 'cancel',
              handler: () => {
                this.nav.pop();
              }
            }
          ]
        });
        alert.present();
      }, (error) => {
        var errorMessage: string = error.message;
        let errorAlert = this.alertCtrl.create({
          message: errorMessage,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        errorAlert.present();
      });
    }
  }

}
