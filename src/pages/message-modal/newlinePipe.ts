import {Pipe, Injectable} from '@angular/core'

@Pipe({
  name: 'prettyPrint'
})
@Injectable()
export class PrettyPrint {

    transform(str: string) {
        return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
    }

}