import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
//import { Auth } from '@ionic/cloud-angular'; //User, UserDetails, IDetailedError, Push, PushToken 
import { WindowService } from '../../providers/window-service';
import { DatastreamService} from '../../providers/datastream-service';
import { ManagementPage } from '../management/management';
import { OfferPage } from '../offer/offer';
import { RequestPage } from '../request/request';
import { SettingsPage } from '../settings/settings';
import { LoginPage } from '../login/login';
import { OfferPendingReservedPage } from '../offerPendingReserved/offerPendingReserved';
import { RequestPendingReservedPage } from '../requestPendingReserved/requestPendingReserved';
import { AppVersion } from '@ionic-native/app-version';
import { AlertController } from "ionic-angular";
import { NotificationProvider } from '../../providers/notification-provider';
import { Badge } from '@ionic-native/badge';
import { ConversationsPage } from '../conversations/conversations';
/*
  Generated class for the Landing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-landing',
  templateUrl: 'landing2.html'
})
export class Landing2Page {
  appName: String;
  packageName: String;
  versionNumber: String;
  versionCode: String;
  badges: number;
  nospot: boolean = true;
  spot: String;
  imgSrc_Offer: String;
  imgSrc_Find: String;
  imgSrc_Requests: String;
  imgSrc_Profile: String;
  imgSrc_Chat: String;
  imgSrc_Settings: String;


  username: String;
  private debugMode: boolean = false;
  constructor( 
    public navCtrl: NavController,
    public navParams: NavParams,
    private windowService: WindowService,
    //private auth: Auth,
    private app: AppVersion,
    private alertCtrl: AlertController, 
    private notificationprovider: NotificationProvider,
    private badge: Badge,
    private platform: Platform, 
    private datastream: DatastreamService) 
    
    {
    this.datastream.initializeStreams();
    this.username = windowService.window.localStorage.getItem('user');
    this.spot = windowService.window.localStorage.getItem('spot');
    this.spot = this.windowService.getKey('spot', '');
    //this.auth = navParams.data;
    this.imgSrc_Offer = "assets/svg/Neighborly_OfferSpot.svg";
    this.imgSrc_Find = "assets/svg/Neighborly_FindASpot.svg";
    this.imgSrc_Requests = "assets/svg/Neighborly_Requests.svg";
    this.imgSrc_Profile = "assets/svg/Neighborly_Profile.svg";
    this.imgSrc_Chat = "assets/svg/Neighborly_Chat.svg";
    this.imgSrc_Settings = "assets/svg/Neighborly_Settings.svg";
    if (this.debugMode) {
      console.log("entering landing page");

    }
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');

  }

  ionViewWillEnter() {
         //this runs every time that the page is entereed.  
             if (this.platform.is("cordova")) {
      this.badge.get().then((x) => {
        this.badges = x;
      });
    }
    console.log("IonViewWillEnter");

  }
 


   logOut() {
            //this.notificationprovider.unregister();  //moved to settings
            // AUTH AUTH AUTH AUTH
            // this.auth.logout();
            this.navCtrl.push(LoginPage); 
   }

  goToOffer() {
    this.navCtrl.push(OfferPage);
  }

  goToRequest() {
    this.navCtrl.push(RequestPage);
  }

  goToConversations() {
    //this.imgSrc = "assets/svg/Neighborly_Chat2.svg"
    this.navCtrl.push(ConversationsPage);
  }

  changePic(pic_num) {

    if (this.imgSrc_Offer == "assets/svg/Neighborly_OfferSpot.svg" && pic_num == 1){
      this.imgSrc_Offer = "assets/svg/Neighborly_OfferSpot2.svg"
    } else {
      this.imgSrc_Offer = "assets/svg/Neighborly_OfferSpot.svg"
    }

    if (this.imgSrc_Find == "assets/svg/Neighborly_FindASpot.svg" && pic_num == 2){
      this.imgSrc_Find = "assets/svg/Neighborly_FindASpot2.svg"
    } else {
      this.imgSrc_Find = "assets/svg/Neighborly_FindASpot.svg"
    }

    if (this.imgSrc_Requests == "assets/svg/Neighborly_Requests.svg" && pic_num == 3){
      this.imgSrc_Requests = "assets/svg/Neighborly_Requests2.svg"
    } else {
      this.imgSrc_Requests = "assets/svg/Neighborly_Requests.svg"
    }

    if (this.imgSrc_Profile == "assets/svg/Neighborly_Profile.svg" && pic_num == 4){
      this.imgSrc_Profile = "assets/svg/Neighborly_Profile2.svg"
    } else {
      this.imgSrc_Profile = "assets/svg/Neighborly_Profile.svg"
    }
    
    if (this.imgSrc_Chat == "assets/svg/Neighborly_Chat.svg" && pic_num == 5){
      this.imgSrc_Chat = "assets/svg/Neighborly_Chat2.svg"
    } else {
      this.imgSrc_Chat = "assets/svg/Neighborly_Chat.svg"
    }

    if (this.imgSrc_Settings == "assets/svg/Neighborly_Settings.svg" && pic_num == 6){
      this.imgSrc_Settings = "assets/svg/Neighborly_Settings2.svg"
    } else {
      this.imgSrc_Settings = "assets/svg/Neighborly_Settings.svg"
    }
  }

  goToManagement() {
    this.navCtrl.push(ManagementPage);
  }

  goToSettings() {
    this.navCtrl.push(SettingsPage);
  }

  goToOfferPending() {
    this.navCtrl.push(OfferPendingReservedPage);
  }

  goToRequestPending() {
    this.navCtrl.push(RequestPendingReservedPage);
  }
}