// This import loads the firebase namespace along with all its type information.
import * as firebase from 'firebase/app';
// These imports load individual services into the firebase namespace.
import 'firebase/auth';
import 'firebase/database';
import { FirebaseObjectObservable } from 'firebase/database';
import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireAction, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'page-sandbox',
  templateUrl: 'sandbox.html'
})
export class SandboxPage {
// Query by list
  items$: Observable<AngularFireAction<firebase.database.DataSnapshot>[]>;
  size$: BehaviorSubject<string|null>;  
  items: Observable<any[]>;

// Query by object
  itemRef: AngularFireObject<any>;
  item: FirebaseObjectObservable<any>;

  constructor(db: AngularFireDatabase) {
// Query by list
    this.size$ = new BehaviorSubject(null);
    this.items$ = this.size$.switchMap(size =>
      db.list('/items', ref =>
        size ? ref.orderByChild('size').equalTo(size) : ref
      ).snapshotChanges()
    );


// Query by object
    this.itemRef = db.object('items');
    console.log(this.itemRef)
    this.item = this.itemRef.valueChanges();
    console.log(this.item)

// using snapshotChanges  <---- This seems to be what we want for some of the "key" code
    this.itemRef = db.object('items');
    this.itemRef.snapshotChanges().subscribe(result => {
      console.log("******")
      console.log(result.type);
      console.log(result.key)
      console.log(result.payload.val())
      console.log(JSON.stringify(result.payload.val()))
      console.log(result.payload.val());
      console.log(Object.keys(result.payload.val()));
      var keys = Object.keys(result.payload.val());
      console.log(result.payload.val()[keys[0]]);
      console.log(result.payload.val()['b']);
      console.log(result.payload.val()['b']['size']);
    });

// using snapshotChanges to get invitation codes
    this.itemRef = db.object('invitations/MattsInvite');
    this.itemRef.snapshotChanges().subscribe(result => {
      console.log("------")
      console.log(result.type);
      console.log(result.key)
      console.log(result.payload.val())
      console.log(JSON.stringify(result.payload.val()))
    });
  }
  filterBy(size: string|null) {
    this.size$.next(size);
  }
}





