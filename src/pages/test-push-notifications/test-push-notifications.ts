import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { HttpClient } from '@angular/common/http/';
import { HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the TestPushNotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test-push-notifications',
  templateUrl: 'test-push-notifications.html',
})

export class TestPushNotificationsPage {
myOwnToken: String;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private fcm: FCM,
    public http: HttpClient) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPushNotificationsPage');


    this.fcm.getToken().then(token=>{
      alert("token is "+token);
      console.log("token is:");
      console.log(token);
      this.myOwnToken = token;
      })

    this.fcm.onNotification().subscribe(data=>{
        alert(JSON.stringify(data));
        console.log("data received");
        console.log(data);
        console.log(data.title);
        console.log(JSON.stringify(data));
        if(data.wasTapped){
          alert("Received in background");
        } else {
          alert("Received in foreground");
        };
      },error =>{
        alert("error is "+JSON.stringify(error));
      })

  }



  sendNotification() {  
    let toUser = this.myOwnToken;
    let notificationMsg = "beer";
    let notifType = "beerbeer";
    let data = "datadata";
    let number = 1;
    let link = "https://fcm.googleapis.com/fcm/send";

let body = {
    "notification":
    {
      "title":"New Notification has arrived",  //the notification section is what is used in the android messaging tray.
      "body":"Notification Body",
      "sound":"default",
      "click_action":"FCM_PLUGIN_ACTIVITY",
      "icon":"fcm_push_icon"
    },
    "data":
    {
      "title":"This is some serious bullcrap",   //the data that you want to extract on the receiving end goes into the "data" section.
      "body":"Srsly. bullcrap.",               //it seems that you can't touch what's in the 'notification' section
      "sound":"default",
    },
      "to": this.myOwnToken,
      "priority":"high",
      "restricted_package_name":""
  }

// let body = {
//       "title": "Neighborly",
//        "to": toUser,
//        "profile": "push_notification",
//        "content-available": 1,
//        "notification": {
//          "content-available": 1,
//          "message": notificationMsg,
//          "android": {
//            "content-available": 1,
//            "title": notifType,
//            "payload": { data }
//          },
//          "ios": {
//            "content-available": 1,
//            "sound": "default",
//            "title": notifType,
//            "badge": number + 1,  // I set this to one for the case where we need to use the ionic cloud notification system.  
//            // This way, a message will show "1" and then it needs to be cleared in another screen
//            "payload": { data }
//          }
//        }
//      }

  let options = new HttpHeaders().set('Content-Type','application/json');
  this.http.post(link ,body,{
    headers: options.set('Authorization', 'key=AAAAF2Ounfs:APA91bG8mwFesopsdAvFz1rXAxLrXF9JMq5iNw4VOvCjihhiBLJA336kdcyxCpOkjCe9Wa6tzIVrSmJBNuyKO81x2i3nLqH51AUYwjda6Rqm4yW-HFp6o9X5l-PPyGj_wd5UEBnA5CZw'),
    })
    .subscribe((res: any) => {
      console.log(res)
    }, 
    (err) => {
      console.log(err);
    })
  }


sendNotificationWillIos() // iphone:  
  {
    let toUser = "cX7qeOriq8c:APA91bHbXWXEKuw1hjXeIjijUvxVtr-970z_7utoiXhqRWsbICtybPJUzYdRGlXvdFX29Q-LiDvvJktFZKQaZoXchJLyZ8Ch1HGru34CjzICqOVt6lQrd_H1XYafaRXFpvB0LiEoV7Jv"
    ;
    let notificationMsg = "beer";
    let notifType = "beerbeer";
    let data = "datadata";
    let number = 1;
    let link = "https://fcm.googleapis.com/fcm/send";
  
    let body = 
    {
      "notification":
      {
        "title":"New Notification has arrived",  //the notification section is what is used in the android messaging tray.
        "body":"Notification Body",
        "sound":"default",
        "click_action":"FCM_PLUGIN_ACTIVITY",
        "icon":"fcm_push_icon"
      },
      "data":
      {
        "title":"This is some serious bullcrap",   //the data that you want to extract on the receiving end goes into the "data" section.
        "body":"Srsly. bullcrap.",               //it seems that you can't touch what's in the 'notification' section
        "sound":"default",
      },
        "to": toUser,
        "priority":"high",
        "restricted_package_name":""
    }
  
    let options = new HttpHeaders().set('Content-Type','application/json');
    this.http.post(link ,body,
      {
      headers: options.set('Authorization', 'key=AAAAF2Ounfs:APA91bG8mwFesopsdAvFz1rXAxLrXF9JMq5iNw4VOvCjihhiBLJA336kdcyxCpOkjCe9Wa6tzIVrSmJBNuyKO81x2i3nLqH51AUYwjda6Rqm4yW-HFp6o9X5l-PPyGj_wd5UEBnA5CZw'),
      })
      .subscribe((res: any) => {
        console.log(res)
      }, 
      (err) => {
        console.log(err);
      })
  }

  sendNotificationWillAndroid() // android:   e0ZLe77e10I:APA91bHUAyuZyA4sV_mCH9r76UKgql5XB-KA-1pIo-fxjdYk6FoxMgo2GLaODts8VpvK1UaJ_FHYj-O09_d8xdQ_gVTGexVPPDyRZwpRXjm-2lV42bnhf326gOX0tK004RJRN_vhZ7gp
  {
    let toUser = "e0ZLe77e10I:APA91bHUAyuZyA4sV_mCH9r76UKgql5XB-KA-1pIo-fxjdYk6FoxMgo2GLaODts8VpvK1UaJ_FHYj-O09_d8xdQ_gVTGexVPPDyRZwpRXjm-2lV42bnhf326gOX0tK004RJRN_vhZ7gp"
    ;
    let notificationMsg = "beer";
    let notifType = "beerbeer";
    let data = "datadata";
    let number = 1;
    let link = "https://fcm.googleapis.com/fcm/send";
  
    let body = 
    {
      "notification":
      {
        "title":"New Notification has arrived",  //the notification section is what is used in the android messaging tray.
        "body":"Notification Body",
        "sound":"default",
        "click_action":"FCM_PLUGIN_ACTIVITY",
        "icon":"fcm_push_icon"
      },
      "data":
      {
        "title":"This is some serious bullcrap",   //the data that you want to extract on the receiving end goes into the "data" section.
        "body":"Srsly. bullcrap.",               //it seems that you can't touch what's in the 'notification' section
        "sound":"default",
      },
        "to": toUser,
        "priority":"high",
        "restricted_package_name":""
    }
  
    let options = new HttpHeaders().set('Content-Type','application/json');
    this.http.post(link ,body,
      {
      headers: options.set('Authorization', 'key=AAAAF2Ounfs:APA91bG8mwFesopsdAvFz1rXAxLrXF9JMq5iNw4VOvCjihhiBLJA336kdcyxCpOkjCe9Wa6tzIVrSmJBNuyKO81x2i3nLqH51AUYwjda6Rqm4yW-HFp6o9X5l-PPyGj_wd5UEBnA5CZw'),
      })
      .subscribe((res: any) => {
        console.log(res)
      }, 
      (err) => {
        console.log(err);
      })
  }

}




