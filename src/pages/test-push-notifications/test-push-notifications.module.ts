import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestPushNotificationsPage } from './test-push-notifications';

@NgModule({
  declarations: [
    TestPushNotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(TestPushNotificationsPage),
  ],
})
export class TestPushNotificationsPageModule {}
