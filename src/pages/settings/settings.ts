import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification-provider';
import { AlertController } from "ionic-angular";
import { AppVersion } from '@ionic-native/app-version';
//import { Auth } from '@ionic/cloud-angular';
import { LoginPage } from '../login/login';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  versionNumber: String;

  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private notificationProvider: NotificationProvider,
    private alertCtrl: AlertController,
    private app: AppVersion
    //private auth: Auth
    ) {
    console.log("entering settings page");
    this.getdata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
    //this.bullshit = true;
  }

  updatepush(){
      if(this.notificationProvider.enable_push){
        this.notificationProvider.register();
      } else {
        this.logout_alert();
      }
  }

  logout_alert() {
    let alert = this.alertCtrl.create({
      title: "Turn off push notifications?",
      subTitle: '',
      cssClass: 'custom-alert-danger',
      message: `<p>Turning push notifications off will prevent your device from receiving notifications 
      when people request your spot or when they approve spots that you've requested</p>
      <p></p>
      <p>Therefore, unless they are a nuisance, it is recommended that you keep them turned on.</p> `,
      buttons: [
        {
          text: 'Turn off push notifications',
          handler: () => {
            alert.dismiss();
            //this.notificationprovider.unregister();  //moved to settings
            this.notificationProvider.enable_push = false;
            this.notificationProvider.unregister();
            //this.navCtrl.push(LoginPage);
          }
        },
        {
          text: 'Keep push notifications on',
          role: 'cancel',
          handler: () => {
            this.notificationProvider.enable_push = true;
          }
        }
      ]
    });
    alert.present();
  }

   getdata() {
    console.log("entering getdata");
    this.app.getVersionNumber().then((versionNumber) => {
      this.versionNumber = versionNumber
      console.log(versionNumber)
    }).catch((err) => {
      console.log(err);
    })
  }
  
  about() {
    let alert = this.alertCtrl.create({
      title: "About Neighborly:",
      subTitle: '',
      cssClass: 'custom-alert-danger',
      message: `<p>Neighborly was created for Metropolitan Place by William Terrill and Leif Brockman.</p>
      <p>We hope you find it useful! </p>
      <p>You are running test version: ` + this.versionNumber + `</p>
      <img src="https://ih1.redbubble.net/image.314292782.4334/flat,550x550,075,f.u1.jpg" style="width: 50%; height: 50%"/>`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  logOut() {
            //this.notificationprovider.unregister();  //moved to settings
            // AUTH AUTH AUTH AUTH
            // this.auth.logout();
            this.navCtrl.push(LoginPage); 
   }

}
