import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
//import { Http } from '@angular/http'; //Headers
import { HttpClient } from '@angular/common/http';
//import { Database, Auth, User, UserDetails } from '@ionic/cloud-angular';
import { ConversationsDatabase } from '../../providers/conversations-database';
import { MessageModalPage } from '../message-modal/message-modal';
import { MessagingPage } from '../messaging/messaging';
import 'rxjs/add/operator/map';
//import { FirebaseDatabase } from '../../providers/firebase-database';

/*
  Generated class for the Conversations page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html'
})
export class ConversationsPage {


  //*************VARIABLES*************//
  //conversationList: any;
  currentUser: any;
  firebaseconversationList: any;
  private debugMode: boolean = false;

  //*************CONSTRUCTOR*************//
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public http: HttpClient, public conversationDb: ConversationsDatabase) {

    //this.listItems = new Array();
    //this.users = new Array();
    //this.initializeListItems();
    this.initializeConversationsItems();
    console.log("entering conversations page");
  }


  //-----------BEGIN METHODS-----------//

  initializeConversationsItems() {
    //this.conversationDb.conversationsConnect(); // connects to the database.  I'm not really sure it's needed, actually
    this.conversationDb.getConversations(); // this queries the firebase database, manipulates the result with .map and puts the results in firebaseConvStream$
    this.conversationDb.conversationStream$.subscribe(
      //firebaseConvStream$ is a manipulated observable (through the .map() fuction) created in firebase-database from the data returned from firebase
      onNext => {
        this.firebaseconversationList = onNext;
        console.log("firebase conversation subscription: ", this.firebaseconversationList)
      },
      onError => {
        console.log("onError: ", onError)
      },
      () => { }
    )
  }

  itemSelected(listItem) { // this is invoked when the name of the list is pressed and it brings up the Modal page.
    let msgModal = this.modalCtrl.create(MessageModalPage, listItem);
    msgModal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConversationsPage');
  }

  newChat() {
    this.navCtrl.push(MessagingPage);
  }
}
