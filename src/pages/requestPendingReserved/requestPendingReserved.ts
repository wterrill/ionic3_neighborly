import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { WindowService } from '../../providers/window-service';
import { AuthTestProvider } from '../../providers/auth-test/auth-test';
import { GenericDatabase } from '../../providers/generic-database';
import { DatastreamService } from '../../providers/datastream-service';
import { AlertController } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification-provider';
import { ReservationHandler } from '../../components/reservationhandler';
import { Badge } from '@ionic-native/badge';
import * as moment from 'moment';
import { UserInfoPage } from '../userinfo/userinfo'

@Component({
  selector: 'page-requestPendingReseved',
  templateUrl: 'requestPendingReserved.html'
})
export class RequestPendingReservedPage {

  username: String;
  private debugMode: boolean = false;
  private totalPendingSpaces: number = 0;
  private totalReservedSpaces: number = 0;

  private pendingSpotsStream$: any[]
  private reservedSpotsStream$: any[]
  private ionicServe: boolean = false;
  private num_garages: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private windowService: WindowService,
    private datastream: DatastreamService, 
    private alertCtrl: AlertController, 
    private notificationProvider: NotificationProvider,
    private database: GenericDatabase, 
    private badge: Badge, 
    private platform: Platform, 
    private auth: AuthTestProvider) {

    //INITIALIZATION//
    if (!this.platform.is("cordova")) {
      if (this.debugMode) {
        console.log("running ionic serve");
      }
      this.ionicServe = true;
    }
    else {
      if (this.debugMode) {
        console.log("not running ionic serve");
      }
    }
    if (!this.ionicServe) {
      this.badge.clear(); //clear the badge on the launch icon
    }
    this.username = windowService.window.localStorage.getItem('user');
    this.queryPendingSpots();
    //this.queryReservedSpots();
    console.log("entering REQUESTpendingReseved page");
    this.num_garages = parseInt(this.windowService.getKey("num_garages","1"));
  }
  //END INITIALIZATION//

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingReservedPage');
  }

  userpage(user){
    this.navCtrl.push(UserInfoPage,user);
  }

  queryPendingSpots() {

    this.pendingSpotsStream$ = new Array();
    this.reservedSpotsStream$ = new Array();

    this.datastream.requestPendingReservedSpotsStream$.map(response => {
      if (this.debugMode) {
        console.log("the requestPendingReserved page requestPendingReservedSpotStream$ provided observable: ", response.length);
        console.log(response)
      }
      return response;
    }).subscribe(filteredSpots => {
      if (this.debugMode) {
        console.log("The spots after mapping the dataStream are: ");
        console.log(filteredSpots);
      }
      setTimeout(() => {
        //this.loading.dismiss();

        this.totalPendingSpaces = filteredSpots.totalPendingSpots;
        this.totalReservedSpaces = filteredSpots.totalReservedSpots;
        this.pendingSpotsStream$ = filteredSpots.pendingSpotsRefined;
        this.reservedSpotsStream$ = filteredSpots.reservedSpotsRefined;;

        //this.database.update_DB_childByParent("/users/", this.username.replace(/\./g,","), "badge", this.pendingSpotsStream$.length);
        // if (!this.ionicServe) {
        //   this.badge.set(this.pendingSpotsStream$.length);
        // }
      }, 500);
    }, err => {
      console.log("Error with the offers data stream");
      console.log(err);
    })
  }//end of queryPendingSpots


  /**
   * Confirmation of cancellation for a PENDING spot.
   * 
   * @param spot 
   * @param i 
   */

  showConfirmPendCancel(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Double checking...',
      message: 'Are you sure that you want to cancel your pending reservation?',
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'No',
          handler: () => {
            if (this.debugMode){console.log('Disagree clicked');}
          }
        },
        {
          text: 'Yes',
          handler: () => {
            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);

            //Call library function to perform all cancelation functions
            if (this.debugMode) {
              console.log("spot printed from pendingReserved");
              console.log(spot)
              console.log(spot.spot);
              console.log(spot.offer_start);
            }
            this.database.update_DB_childByParent('/buildings/' + spot.building + '/' + spot.garage + "/spots/" + spot.spot + "/offers/", "offer_" + spot.offer_start, "offer_status", 1);

            setTimeout(() => {
              this.queryPendingSpots();
            }, 500);
          }
        }
      ]
    });
    confirm.present();
  }


  /**
   * Confirmation handler for cancellation of
   * a RESERVED spot.
   * 
   * @param spot 
   * @param i 
   */
  showConfirmResCancel(spot, i) {
    let confirm = this.alertCtrl.create({
      title: 'Wait a second...',
      message: 'Are you sure that you want to cancel your existing reservation?',
      cssClass: 'custom-alert-check',
      buttons: [
        {
          text: 'No',
          handler: () => {
            if (this.debugMode){console.log('Disagree clicked');}
          }
        },
        {
          text: 'Yes',
          handler: () => {

            //delete the item from the page
            this.pendingSpotsStream$.splice(i, 1);

            //Call library function to perform all deny functions
            if (this.debugMode) {
              console.log("Renter cancellation request confirmed for the spot: ", spot, + " requestPendingReserved.ts ~ line 408 ");
            }

            let resHandler = new ReservationHandler(this.alertCtrl, this.database, this.auth);

            resHandler.renterCancel(spot).then(result => {
              if (this.debugMode) {
                console.log('Successfully returned from the renter cancellation');
              }

              //let start: String = moment.utc(this.startingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              //let end: String = moment.utc(this.endingMsec).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();

              let start: String = moment(spot._3_request_start).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();
              let end: String = moment(spot._4_request_end).local().format('ddd, MMM Do YYYY [at] h:mm a').toString();

              let notificationMessage = this.username + ' cancelled their reservation to use your spot from \n' +
                start + ' \n To: \n' + end + '\nWe have remade your spot available for rental' +
                ' during the cancelled time.  Please check your offers for more information.'

              let htmlMessage = `<p>` + this.username + ` cancelled their reservation to use your spot from: `  +
                                `<p>` + start + ` </p><p>To:</p> <p>` + end + `</p>
                                <p> We have made your spot available for rental during the cancelled time.</p>
                                <p>Please check your offers for more information.</p>`

              var payload = { 
                "spot": spot,
                "htmlMessage": htmlMessage
              }

              this.notificationProvider.sendNotification(spot.owner, notificationMessage, "notify", payload);

              setTimeout(() => {
                this.queryPendingSpots();
              }, 500);


            }).catch(err => {
              if (this.debugMode) {
                console.log('Something went wrong when the renter tried to cancel.  Err code: ', err);
              }
              let alert = this.alertCtrl.create({
                title: '>_^',
                message: 'I failed to complete the cancellation.  Please try again.',
                cssClass: 'custom-alert-danger',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      console.log('Alert dismissed.');
                      alert.dismiss();
                    }
                  }]
              });
              alert.present();;
            });

            //this.database.update_DB_childByParent("/spots/" + spot.spot + "/offers/", "offer_" + spot.offer_start, "offer_status", 1 );

            // this.queryReservedSpots();

          }
        }
      ]
    });
    confirm.present();
  }

}
