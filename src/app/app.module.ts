import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

// pages
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { ImagemapPage } from '../pages/imagemap/imagemap';
import { SvgmapPage } from '../pages/svgmap/svgmap';
import { SvgzoomPage } from '../pages/svgzoom/svgzoom';
import { MapPage } from '../pages/map/map';
import { MessagingPage } from '../pages/messaging/messaging';
import { MessageModalPage } from '../pages/message-modal/message-modal';
import { ConversationsPage } from '../pages/conversations/conversations';
import { LandingPage } from '../pages/landing/landing';
import { Landing2Page } from '../pages/landing2/landing2';
import { OfferPage } from '../pages/offer/offer';
import { RequestPage } from '../pages/request/request';
import { ManagementPage } from '../pages/management/management';
import { SettingsPage } from '../pages/settings/settings';
import { OfferPendingReservedPage } from '../pages/offerPendingReserved/offerPendingReserved';
import { RequestPendingReservedPage } from '../pages/requestPendingReserved/requestPendingReserved';
import { PasswordResetPage } from '../pages/passwordReset/passwordReset';
import { InvitationPage } from '../pages/invitation/invitation';
import { UserInfoPage } from '../pages/userinfo/userinfo';
import { QueryPage } from '../pages/query/query';
import { SandboxPage } from '../pages/sandbox/sandbox';
import { LoginTestPage } from '../pages/login-test/login-test';
import { SignupTestPage } from '../pages/signup-test/signup-test';
import { ResetPasswordTestPage } from '../pages/reset-password-test/reset-password-test';
import { TestPushNotificationsPage } from '../pages/test-push-notifications/test-push-notifications'


//page components
import { PrettyPrint } from '../pages/message-modal/newlinePipe';
import { ZoomableImage } from 'ionic-gallery-modal';
import { GalleryModal } from 'ionic-gallery-modal';


//services
//import { AuthService } from '../providers/auth-service';
import { GenericDatabase } from '../providers/generic-database';
import { MessagesDatabase } from '../providers/messages-database';
import { ConversationsDatabase } from '../providers/conversations-database';
import { WindowService } from '../providers/window-service';
import { DatastreamService } from '../providers/datastream-service';
import { NotificationProvider } from '../providers/notification-provider';
import { AuthTestProvider } from '../providers/auth-test/auth-test';  //test test test test test

import { Badge } from '@ionic-native/badge';
import { AppVersion } from '@ionic-native/app-version';
import { FCM } from '@ionic-native/fcm';



//components
import { ImgMapComponent } from 'ng2-img-map';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

//added for upgrade to ionic 3:
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Needed for animations

// Validators
import { SpotExistsValidator } from '../validators/spotExistsValidator';


// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyAdn1iBQFMr4oRJVA6YXUDfDo1umxeFrEQ",
  authDomain: "neighborly-577c2.firebaseapp.com",
  databaseURL: "https://neighborly-577c2.firebaseio.com",
  projectId: "neighborly-577c2",
  storageBucket: "neighborly-577c2.appspot.com",
  messagingSenderId: "100456635899",
  preserveSnapshot: 'true'
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage,
    ImagemapPage,
    SvgmapPage,
    SvgzoomPage,
    MapPage,
    ImgMapComponent,
    MessagingPage,
    MessageModalPage,
    ConversationsPage,
    LandingPage,
    Landing2Page,
    OfferPage,
    RequestPage,
    ManagementPage,
    SettingsPage,
    OfferPendingReservedPage,
    RequestPendingReservedPage,
    PasswordResetPage,
    InvitationPage,
    UserInfoPage,
    PrettyPrint,
    GalleryModal,
    ZoomableImage,
    QueryPage,
    SandboxPage,
    LoginTestPage,
    SignupTestPage,
    ResetPasswordTestPage,
    TestPushNotificationsPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    //CloudModule.forRoot(cloudSettings), removed for upgrade to ionic 3
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    MessagingPage,
    MessageModalPage,
    LoginPage,
    ConversationsPage,
    MapPage,
    SvgzoomPage,
    ImagemapPage,
    SvgmapPage,
    LandingPage,
    Landing2Page,
    OfferPage,
    RequestPage,
    ManagementPage,
    SettingsPage,
    OfferPendingReservedPage,
    RequestPendingReservedPage,
    GalleryModal,
    QueryPage,
    PasswordResetPage,
    InvitationPage,
    UserInfoPage,
    SandboxPage,
    LoginTestPage,
    SignupTestPage,
    ResetPasswordTestPage,
    TestPushNotificationsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //AuthService, 
    MessagesDatabase, 
    ConversationsDatabase, 
    WindowService, 
    GenericDatabase, 
    DatastreamService, 
    NotificationProvider,
    SpotExistsValidator,
    Badge,
    AppVersion,
    AngularFireDatabase,
    AuthTestProvider,  //test test test test
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthTestProvider,
    FCM
  ]
})
export class AppModule {}


/*



import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//Added by Will
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';




import { AppVersion } from '@ionic-native/app-version';




















// I think this is old code?  Otherwise how would our notifications ever work?

// App ID chould be a CONST but I don't know if Sender ID shoudl be... isn't that tied to your device token?
// Just trying to wrap my head back around these things....

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '9a7f489c',
  },
  'push': {
    'sender_id': '100456635899',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    
    MessagingPage,
    MessageModalPage,
    
    ConversationsPage,
    MapPage,
   


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
   
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler },  
                //AuthService, 
                MessagesDatabase, 
                ConversationsDatabase, 
                WindowService, 
                GenericDatabase, 
                DatastreamService, 
                NotificationProvider,
                Badge,
                AppVersion,
                SpotExistsValidator
                //PhotoViewer
                ]
})
export class AppModule { }

*/

