import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

import 'web-animations-js/web-animations.min';  //added to support animations during upgrade to ionic 3

platformBrowserDynamic().bootstrapModule(AppModule);
