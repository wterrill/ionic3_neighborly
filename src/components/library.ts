
export class DebugMessage {
    debugOnOff: boolean = true;
    user: string;
    page: string;
    info: string;
    datetime: string;
    comment: string;

    constructor(user: string, page: string, info: string, datetime: string, comment: string
    ) {
        this.user = user;
        this.page = page;
        this.info = info;
        this.datetime = datetime;
        this.comment = comment;
    }
}


export class BUILDINGS {
    static METROPOLITAN_PLACE = { value: 1, name: 'metropolitan', BUILDING_DATABASE: 'buildings/metropolitan', ADAMS_DATABASE: 'buildings/metropolitan/adams/spots', CANAL_DATABASE: 'buildings/metropolitan/canal/spots' };
    static TERRYAKI_PLACE = { value: 2, name: 'terryaki', BUILDING_DATABASE: 'buildings/terryaki', GARAGE_DATABASE: 'buildings/terryaki/garage1/spots' };
    static ELEVEN_ELEVEN_W_MADISON = { value: 3, name: '1111WMadison', BUILDING_DATABASE: 'buildings/1111WMadison', GARAGE_DATABASE: 'buildings/1111WMadison/garage1/spots' };
}


export class Transaction {
    _1_offer_start: string;
    _2_offer_end: string;
    _3_request_start: string;
    _4_request_end: string;
    building: string;
    garage: string;
    offer_end: number;
    offer_start: number;
    offer_status: number;
    offer_weight: number;
    owner: string;
    request_end: number;
    request_start: number;
    request_user: string;
    request_weight: number;
    spot: string

    constructor(_1_offer_start: string, _2_offer_end: string, _3_request_start: string, _4_request_end: string, building: string,
        garage: string, offer_end: number, offer_start: number, offer_status: number, offer_weight: number,
        owner: string, request_end: number, request_start: number, request_user: string,
        request_weight: number, spot: string) {
        this._1_offer_start = _1_offer_start;
        this._2_offer_end = _2_offer_end;
        this._3_request_start = _3_request_start;
        this._4_request_end = _4_request_end;
        this.building = building;
        this.garage = garage;
        this.offer_end = offer_end;
        this.offer_start = offer_start;
        this.offer_status = offer_status;
        this.offer_weight = offer_end - offer_start;
        this.owner = owner;
        this.request_end = request_end;
        this.request_start = request_start;
        this.request_user = request_user;
        this.request_weight = request_end - request_start;
        this.spot = spot;
    }

    _cloneTransaction(transactionToClone: Transaction): Transaction {
        return new Transaction(transactionToClone._1_offer_start, transactionToClone._2_offer_end, transactionToClone._3_request_start, transactionToClone._4_request_end,
            transactionToClone.building, transactionToClone.garage, transactionToClone.offer_end, transactionToClone.offer_start,
            transactionToClone.offer_status, transactionToClone.offer_weight, transactionToClone.owner, transactionToClone.request_end,
            transactionToClone.request_start, transactionToClone.request_user, transactionToClone.request_weight, transactionToClone.spot);
    }

}

export class Reservation {
    _1_reservation_start: string;
    _2_reservation_end: string;
    building: string;
    garage: string;
    reservation_start: number;
    reservation_end: number;
    reservation_weight: number;
    reservation_confirmed: string;
    reservation_cancelled: string;
    reservation_status: number;
    renter: string;
    spot: string;

    constructor(_1_reservation_start: string, _2_reservation_end: string, building: string, garage: string, reservation_start: number, reservation_end: number,
        reservation_confirmed: string, reservation_cancelled: string, reservation_status: number,
        renter: string, spot: string) {

        this._1_reservation_start = _1_reservation_start;
        this._2_reservation_end = _2_reservation_end;
        this.building = building;
        this.garage = garage;
        this.reservation_start = reservation_start;
        this.reservation_end = reservation_end;
        this.reservation_weight = this.reservation_end - this.reservation_start;
        this.reservation_confirmed = reservation_confirmed;
        this.reservation_cancelled = reservation_cancelled
        this.renter = renter;
        this.spot = spot;
    }
    /**
     * Takes a reservation and 
     * @param reservationToClone The reservation you wish to clone.
     */
    _cloneReservation(reservationToClone: Reservation): Reservation {
        return new Reservation(reservationToClone._1_reservation_start, reservationToClone._2_reservation_end, reservationToClone.building, reservationToClone.garage,
            reservationToClone.reservation_start, reservationToClone.reservation_end, reservationToClone.reservation_confirmed, reservationToClone.reservation_cancelled,
            reservationToClone.reservation_status, reservationToClone.renter, reservationToClone.spot)
    }

}
