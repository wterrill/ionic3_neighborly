
import { GenericDatabase } from "../providers/generic-database";
import { AlertController } from "ionic-angular";
import { AuthTestProvider } from '../providers/auth-test/auth-test';
import { User_local as User } from '../providers/auth-service';
import { Transaction } from './library';
import { BUILDINGS } from './library';
import * as moment from 'moment';

export class OfferHandler {

    private debugMode: boolean = false;
    private userDetails: User;
    constructor(private alertCtrl: AlertController, private database: GenericDatabase, private auth: AuthTestProvider) {

        if (this.debugMode) {
            console.log('Building a new OfferHandler!');
        }

        this.userDetails = auth.getUserInfo();
    }

    /**This function UPDATES AN EXISTING OFFER
     * already present in the database.  Do not
     * use to CREATE NEW OFFERS!!!!.  Will create
     * offers that are already reserved.
     * 
     * @param spot The offer you wish to create
     * as a record into the database.
     * @return returns a promies that indicates that
     * the proper database interaction took place.
     */

    updateOffer(spot): Promise<any> {

        var pathA = this.tableCheck(spot);
        var pathB = spot.spot;
        var path = '/' + pathA + '/' + pathB;     // old example needed a leading '/' to work properly.
        var key = "offers";
        var childId = 'offer_' + spot.offer_start;


        let startDate = new Date(spot.offer_start);
        let endDate = new Date(spot.offer_end);

        spot._1_offer_start = moment(startDate.getTime()).local().toString()
        spot._2_offer_end = moment(endDate.getTime()).local().toString()

        if (this.debugMode) {
            console.log("offerhandler:  Updating the spot : ", spot, 'to be available on ' + spot.offer_start);
            console.log(" Inserting into " + path + ' at the key ' + key + ' and child ' + childId + ' the data: ', spot);
        }

        this.database.update_DB_childByParent(path, key, childId, spot);

        return new Promise(resolve => setTimeout(resolve, 2000))
    }


    /**THIS CREATES A NEW OFFER into the database.
     * DO NOT USE to update an existing offer; it will
     * fail to update the proper field sand make remove
     * the requesting user information.
     * 
     * @param offer the offer you wish to create
     * @return returns a promie that indicates the
     * proper database interaction took place.
     */
    createOffer(offer, status): Promise<any> {

        if (this.debugMode) {
            console.log("Offering the spot : ");
            console.log(offer);
            console.log(" to be availble to rent beginning on " + offer.offer_start);
        }

        let startDate = new Date(offer.offer_start);
        let endDate = new Date(offer.offer_end);

        const BUILDING = offer.building;
        const GARAGE = offer.garage;
        if (offer.owner === " " || offer.owner === null || offer.owner === undefined || offer.owner === "") {
            var newOffer = new Transaction(moment(startDate.getTime()).local().toString(), moment(endDate.getTime()).local().toString(), ' ', ' ',
                BUILDING, GARAGE, offer.offer_end, offer.offer_start, status, offer.offer_end - offer.offer_start,
                this.userDetails.email, -1, -1, ' ', -1, offer.spot);
        } else {
            var newOffer = new Transaction(moment(startDate.getTime()).local().toString(), moment(endDate.getTime()).local().toString(), ' ', ' ',
                BUILDING, GARAGE, offer.offer_end, offer.offer_start, status, offer.offer_end - offer.offer_start,
                offer.owner, -1, -1, ' ', -1, offer.spot);
        }
        var table = this.tableCheck(offer);

        var record = offer.spot + "/offers/offer_" + offer.offer_start;

        this.database.createEntry(table, record, newOffer);

        return new Promise(resolve => setTimeout(resolve, 2000))
    }



    /**
     * Checks to make sure that a duplicate entry is not trying to be added to the database.
     * Checks a proposed offer and will resolve() if that offer does not exist;
     * if that offer already exists (of if the proposed offer overlaps in some way another offer)
     * it will reject()
     * 
     * @param offer The offer that must be checked.
     */
    offerCheck(offer): Promise<any> {

        return new Promise((resolve, reject) => {

            var validProposal: boolean = true;

            var tableA = this.tableCheck(offer);
            var tableB = offer.spot;        // OFFERS.SPOT FORMAT IS SPOT_NUMBER

            var table = tableA + '/' + tableB;
            if (this.debugMode){console.log('Offer check table combo', table);}
            var item = 'offers'

            this.database.queryDB_single_item_as_Promise(table, item).then(allConfirmedOffers => {
                if (this.debugMode) {
                    console.log("All currently made offers for the spot as found by offerCheck ", allConfirmedOffers);
                }

                /* Check to see if the current offer overlaps any made offers for the current spot.
                We do this by checking all the confirmed offers for the current spot (those which 
                have been recorded in the database) to see if one of two conditions exist producing
                an invalid proposal...

                If the proposed start falls between any confirmed offers start and end time, they overlap,
                    example of an invalid timeline:
                        allConfirmedOffer[i].offer_start ..... offer.offer_start ...... allConfirmedOffers[i].offer_end ..... offer.offer_end
                           6/26 5pm                                 6/26 7pm                       6/26 9pm                      6/26 11 pm        

                If the proposed end time falls between any confirmed offers start and end time, they overlap,
                    example of an invalid timeline:
                        offer.offer_start ..... allConfirmedOffers[i].offer_start ..... offer.offer_end ....... alllConfirmedOffers[i].offer_end
                           6/26 5pm                     6/26 7pm                            6/26 9pm                      6/26 11 pm      

                If the proposed time envelopes an existing offer that offer is not valid.
                    example of an invalid timeline:
                        offer.offer_start ..... allConfirmedOffers[i].offer_start ..... allConfirmedOffers[i].offer_end ...... offer.offer_end
                           6/25 6pm                     6/25 8 pm                          6/25 11pm                           6/26 8 am

                It is valid for a allConfirmedOffers[i].offer_end to be equal to offer.start, and for offer.offer_end to be equal to allConfirmedOffers[i].offer_start        
                */
                if (!allConfirmedOffers){
                    allConfirmedOffers=[] //This was added for the new angularfire2 that returns null instead of an empty array
                } 

                var keys = Object.keys(allConfirmedOffers)

                for (var i = 0; i < keys.length; i++) {
                        
                    if (allConfirmedOffers[keys[i]].offer_start <= offer.offer_start && allConfirmedOffers[keys[i]].offer_end > offer.offer_start) {
                        validProposal = false;
                        break;
                    } else if (offer.offer_end > allConfirmedOffers[keys[i]].offer_start && offer.offer_end <= allConfirmedOffers[keys[i]].offer_end) {
                        validProposal = false;
                        break;
                    } else if (allConfirmedOffers[keys[i]].offer_start >= offer.offer_start && allConfirmedOffers[keys[i]].offer_end <= offer.offer_end) {
                        validProposal = false;
                        break;
                    }
                }

                if (validProposal) {

                    if (this.debugMode) {
                        console.log('the offer does not yet exist');
                    }

                    resolve();

                } else {

                    if (this.debugMode) {
                        console.log('the offer already exists');
                    }

                    reject();

                }

            }).catch(() => {
                reject();
            });

        });

    }

    public tableCheck(spot): string {

        const BUILDING = spot.building;
        const GARAGE = spot.garage;

        var table = '';

        if (BUILDING == 'metropolitan') {
            if (GARAGE == 'adams') {
                table = BUILDINGS.METROPOLITAN_PLACE.ADAMS_DATABASE;
            }
            else {
                table = BUILDINGS.METROPOLITAN_PLACE.CANAL_DATABASE;
            }

        } else if (BUILDING == 'terryaki') {
            table = BUILDINGS.TERRYAKI_PLACE.GARAGE_DATABASE;
        } else if (BUILDING == '1111WMadison') {
            table = BUILDINGS.ELEVEN_ELEVEN_W_MADISON.GARAGE_DATABASE;
        } else {
            // default case to handle old 
            // database interaction for
            // short term troubleshooting.
            table = 'spots'
        }

        if (this.debugMode) {
            console.log('offerHanlder tableCheck() produced building: ', BUILDING, ' and table: ', table);
        }

        return table;

    }

}